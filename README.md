# Export your Xcode Project to Cocoapods
<p align="center">
        <img src="http://pic.33.la/allimg/111128/1-11112p54912-50.png" height="200" width="200" />
        <img src="http://cdn1.raywenderlich.com/wp-content/uploads/2015/02/cocoapods_logo.png" height="200" width="200" />
</p>

##How to upload Xcode Project to Cocoapods

####1 Open Terminal 

####2. Go to your project with : 
#####CD document
#####CD projectXcode

####3. In your Terminal add :
#####pod spec create PROJECT_NAME

####4.
#####open -a Xcode PROJECT_NAME.podspec

####5.
#####add this 
```swift 
Pod::Spec.new do |s|
s.name        = "PROJECT_NAME"
s.version     = "1.0.0"
s.summary     = "PROJECT_NAME_DESCRIPTION"
s.homepage    = "https://github.com/NAME/PROJECT_NAME"
s.license     = { :type => "MIT" }
s.authors     = { "MY_NAME" => "MAIL@MAIL.COM" }

s.ios.deployment_target = "8.0"
s.source   = { :git => "https://github.com/NAME/PROJECT_NAME.git", :tag => "1.0.0"}
s.source_files = "FOLDER/CLASS.{swift}", "Classes", "Classes/**/*.{swift}"
s.requires_arc = true
end
```
####6.
#####Save

####7.
#####pod spec lint EasyGameCenter.podspec

####8. Warning ?
#####pod spec lint EasyGameCenter.podspec --allow-warnings

####9.
#####pod lib lint --allow-warnings

####10.
#####pod trunk register stephan.yannick@me.com 'DaRkD0G' --description='Macbook'

####11.
#####pod trunk push --allow-warnings

####12.
#####git add .

####13.
#####git commit -m "Add Cocoapods"

####14.
#####git push origin master


Update 
git tag 1.0.0
git push --tags
